<?php
if (isset($_POST)) {
    if (isset($_POST['merchant_id']) && isset($_POST['order_id']) && isset($_POST['amount'])) {
        require("ConcordPaySDK.php");
        require('../../wp-load.php');

        $wc_googlepay = new WC_googlepay();
        $secretKey = $wc_googlepay->get_option('secret_key');
        $sdk = new ConcordPaySDK($secretKey);
        $status = $sdk->googlePay($_POST);
        $order = new WC_Order($_POST['order_id']);
        $items = $order->get_items();

        if ($status == 'Test') { //change OK
            $order = new WC_Order($_POST['order_id']);
            $order->save();
            $items = $order->get_items();

            foreach (WC()->cart->get_cart() as $cartItem) {
                $product = wc_get_product($cartItem['product_id']);
                wc_get_order($order->get_id())->add_product($product, $cartItem['quantity']);
            }

            $customer_id = get_current_user_id();
            $order->set_total($_POST['amount']);
            $order->set_billing_first_name(get_user_meta($customer_id, 'billing_first_name', true));
            $order->set_billing_last_name(get_user_meta($customer_id, 'billing_last_name', true));
            $order->set_billing_address_1(get_user_meta($customer_id, 'billing_address1', true));
            $order->set_billing_address_2(get_user_meta($customer_id, 'billing_address2', true));
            $order->set_billing_phone(get_user_meta($customer_id, 'phone', true));
            $order->set_shipping_address_1(get_user_meta($customer_id, 'billing_address1', true));
            $order->set_shipping_city(get_user_meta($customer_id, 'city', true));
            $order->set_shipping_first_name(get_user_meta($customer_id, 'shipping_first_name', true));
            $order->set_shipping_last_name(get_user_meta($customer_id, 'shipping_last_name', true));
            $order->set_shipping_address_1(get_user_meta($customer_id, 'shipping_address_1', true));
            $order->set_shipping_address_2(get_user_meta($customer_id, 'shipping_address_2', true));
            $order->set_billing_email(get_user_meta($customer_id, 'billing_email', true));

            $phone = get_user_meta($customer_id, 'billing_phone', true);
            $phone = str_replace(array('+', ' ', '(', ')'), array('', '', '', ''), $phone);
            if (strlen($phone) == 10) {
                $phone = '38' . $phone;
            } elseif (strlen($phone) == 11) {
                $phone = '3' . $phone;
            }
            $order->set_billing_phone($phone);
            $order->set_payment_method('google_pay');
            $order->save();
            $order->update_status('wc-processing');
            WC()->cart->empty_cart();
        } else {

            $order->update_status('wc-pending', 'google_pay');

        }
    }
}

