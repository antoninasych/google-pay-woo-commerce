<?php
/**
 *Plugin Name: WooCommerce - GooglePay
 *Description: GooglePay Payment Gateway for WooCommerce. just add [google_pay] short code into your cart
 *Version: 1.0
 *Author URI: https://pay.concord.ua
 *License: GNU General Public License v3.0
 *License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */


add_action('plugins_loaded', 'woocommerce_googlepay_init', 0);

function woocommerce_googlepay_init()
{
    if (!class_exists('WC_Payment_Gateway')) {
        return;
    }

    if (isset($_GET['msg']) && !empty($_GET['msg'])) {
        add_action('the_content', 'showGooglePayMessage');
    }
    function showGooglePayMessage($content)
    {
        return '<div class="' . htmlentities($_GET['type']) . '">' . htmlentities(urldecode($_GET['msg'])) . '</div>' . $content;
    }

    /**
     * Gateway class
     */
    class WC_googlepay extends WC_Payment_Gateway
    {


        public $merchant_id;
        public $secretKey;
        public $callbackUrl;
        public $approvedUrl;

        public function __construct()
        {
            $this->id = 'googlepay';
            $this->method_title = 'GooglePay';
            $this->method_description = "Payment gateway";
            $this->has_fields = false;

            $this->init_form_fields();
            $this->init_settings();

            $this->title = "GooglePay";


            $this->approvedUrl = $this->settings['approvedUrl'];
            $this->callbackUrl = $this->settings['redirectUrl'];
            $this->merchant_id = $this->settings['merchant_id'];
            $this->secretKey = $this->settings['secret_key'];


        }

        function init_form_fields()
        {
            // fields for settings
            $this->form_fields = array('enabled' => array('title' => __('Enable/Disable', 'kdc'),
                'type' => 'checkbox',
                'label' => __('Enable GooglePay Payment Module.', 'kdc'),
                'default' => 'no',
                'description' => 'Show in the Payment List as a payment option'),
                'merchant_id' => array('title' => __('Merchant Account', 'kdc'),
                    'type' => 'text',
                    'description' => __('Given to Merchant by ConcordBank'),
                    'desc_tip' => true
                ),
                'secret_key' => array('title' => __('Secret key', 'kdc'),
                    'type' => 'text',
                    'description' => __('Given to Merchant by ConcordBank', 'kdc'),
                    'desc_tip' => true,
                ),
                'callbackUrl' => array('title' => __('URL of the result information'),
                    'options' => $this->get_all_pages('Select Page'),
                    'type' => 'select',
                    'description' => __('The URL to which will receive information about the result of the payment', 'kdc'),
                    'desc_tip' => true)
            ,

                'approveUrl' => array('title' => __('Successful payment redirect URL'),
                    'options' => $this->get_all_pages('Select Page'),
                    'type' => 'select',
                    'description' => __('Successful payment redirect URL', 'kdc'),
                    'desc_tip' => true)
            );
        }

        /**
         * Admin Panel Options
         */
        public function admin_options()
        {
            echo '<h3>' . __('Concord.ua', 'kdc') . '</h3>';
            echo '<p>' . __('Payment gateway') . '</p>';
            echo '<table class="form-table">';
            // Generate the HTML For the settings form.
            $this->generate_settings_html();
            echo '</table>';
        }

        // get all pages
        function get_all_pages($title = false, $indent = true)
        {
            $wp_pages = get_pages('sort_column=menu_order');
            $page_list = array();
            if ($title) {
                $page_list[] = $title;
            }
            foreach ($wp_pages as $page) {
                $prefix = '';

                if ($indent) {
                    $has_parent = $page->post_parent;
                    while ($has_parent) {
                        $prefix .= ' - ';
                        $next_page = get_page($has_parent);
                        $has_parent = $next_page->post_parent;
                    }
                }

                $page_list[$page->ID] = $prefix . $page->post_title;
            }
            return $page_list;
        }
    }

    /**
     * Add the Gateway to WooCommerce
     **/
    function woocommerce_add_googlepay_gateway($methods)
    {
        $methods[] = 'WC_googlepay';
        return $methods;
    }

    function gateway_google_pay()
    {
        if(!is_null(WC()->cart) && !empty(WC()->cart->get_cart())){

        $wc_googlepay = new WC_googlepay();
        global $woocommerce;

        $order_id = $order_id . '_woopay_' . time();


        $button = '<div id="container-google-pay-btn"></div>
    <script>

        const tokenizationSpecification = {
            type: "PAYMENT_GATEWAY",
            parameters: {
              "gateway": "concordpay",
                "gatewayMerchantId": "' . $wc_googlepay->merchant_id . '",
                "amount": "' . $woocommerce->cart->total . '",
                "order_id": "' . $order_id . '",
                "callback_url": "' . get_page_link($wc_googlepay->settings['callbackUrl']) . '",
                "approve_url": "' . get_page_link($wc_googlepay->settings['approveUrl']) . '"
 
            },
        };
    </script>
    <script async src="/wp-content/plugins/google.pay.js"></script>
    <script async src="https://pay.google.com/gp/p/js/pay.js" onload="onGooglePayLoaded()"></script>';

        return $button;
        }


        return false;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_add_googlepay_gateway');
    add_shortcode('google_pay', 'gateway_google_pay');
}
