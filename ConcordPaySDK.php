<?php

class ConcordPaySDK
{
    const CURRENCY_UAH = 'UAH';

    protected $supportedCurrencies = array(
        self::CURRENCY_UAH
    );

    protected static $Inst = false;

    const FIELDS_DELIMITER = ';';
    const PURCHASE_URL = 'https://pay.concord.ua/api/';
    const REVERSAL_URL = "https://pay.concord.ua/api/reversal";
    const CHECK_URL = "https://pay.concord.ua/api/check ";
    const BALANCE_URL = "https://pay.concord.ua/api/balance";
    const MASTERPASS_TOKEN_URL = "https://pay.concord.ua/api/mptoken";


    const MODE_GOOGLE_PAY = "GooglePayPurchase";
    const MODE_PURCHASE = 'Purchase';
    const MODE_REC_PAYMENT = 'REC_PAYMENT';
    const MODE_VERIFY = 'VERIFY';
    const MODE_REVERSAL = 'REVERSAL';
    const MODE_COMPLETE = 'COMPLETE';
    const MODE_CHECK = 'CHECK';
    const MODE_P2P_CREDIT = 'P2PCredit';
    const MODE_GET_BALANCE = 'GetBalance';
    const MODE_P2PDEBIT = 'P2PDebit';
    const MODE_CONFIRM_3DS = '3DS';
    const MODE_PURCHASE_ON_MERCHANT = "PurchaseOnMerchant";
    const MODE_GET_TOKEN_MASTER_PASS = "TOKEN_MASTER_PASS";
    const MODE_PURCHASE_MASTER_PASS = "purchaseMasterpass";

    private $_apiUrl = "https://pay.concord.ua/api/";
    private $_privateKey;
    private $_action;
    private $_params;




    /**
     * ConcordPaySDK constructor.
     * @param $privateKey
     * @param null $apiUrl
     */
    public function __construct($privateKey, $apiUrl = null)
    {
        if (empty($privateKey)) {
            throw new InvalidArgumentException('private_key is empty');
        }

        $this->_privateKey = $privateKey;

        if (null !== $apiUrl) {
            $this->_apiUrl = $apiUrl;
        }
    }


    private function _buildForm()
    {
        $form = sprintf('<form method="POST" action="%s" accept-charset="utf-8"  id="sdk-concord">', $this->_apiUrl);

        foreach ($this->_params as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $field) {
                    $form .= sprintf('<input type="hidden" name="%s" value="%s" />', $key . '[]', htmlspecialchars($field));
                }
            } else {
                $form .= sprintf('<input type="hidden" name="%s" value="%s" />', $key, htmlspecialchars($value));
            }
        }

        $form .= '<input type="submit" value="Submit"></form>';

        return $form;
    }

    public function purchase($fields)
    {
        $this->_prepare(self::MODE_PURCHASE, $fields);
        $this->_apiUrl = self::PURCHASE_URL;

        return $this->_buildForm();

    }


    public function recPayment($fields)
    {
        $this->_prepare(self::MODE_REC_PAYMENT, $fields);

        return $this->_query();
    }

    public function verify($fields)
    {
        $this->_prepare(self::MODE_VERIFY, $fields);

        return $this->_query();
    }

    /**
     * @param $fields
     * @return mixed
     */
    public function reversal($fields)
    {
        $this->_prepare(self::MODE_REVERSAL, $fields);
        $this->_apiUrl = self::REVERSAL_URL;

        return $this->_query();
    }

    /**
     * @param array $fields
     * @return mixed
     */
    public function complete($fields)
    {
        $this->_prepare(self::MODE_COMPLETE, $fields);

        return $this->_query();
    }

    /**
     * @param array $fields
     * @return mixed
     */
    public function check($fields)
    {
        $this->_prepare(self::MODE_CHECK, $fields);
        $this->_apiUrl = self::CHECK_URL;

        return $this->_query();
    }

    /**
     * @param array $fields
     * @return mixed
     */
    public function p2pCredit($fields)
    {
        $this->_prepare(self::MODE_P2P_CREDIT, $fields);

        return $this->_query();
    }

    /**
     * @param array $fields
     * @return mixed
     */
    public function getBalance($fields)
    {
        $this->_prepare(self::MODE_GET_BALANCE, $fields);
        $this->_apiUrl = self::BALANCE_URL;

        return $this->_query();
    }

    /**
     * @param array $fields
     * @return mixed
     */
    public function p2pDebit($fields)
    {
        $this->_prepare(self::MODE_P2PDEBIT, $fields);
//      return  $this->_query();

        return $this->_buildForm();
    }

    /**
     * @param  $fields
     * @return mixed
     */
    public function purchaseOnMerchant($fields)
    {
        $this->_prepare(self::MODE_PURCHASE_ON_MERCHANT, $fields);

        return $this->_query();

//        $this->_apiUrl = self::PURCHASE_URL;
//        return $this->buildForm();
    }


    /**
     * @param array $fields
     * @return mixed
     */
    public function getMasterpassToken($fields)
    {
        $this->_prepare(self::MODE_GET_TOKEN_MASTER_PASS, $fields);
        $this->_apiUrl = self::MASTERPASS_TOKEN_URL;

        return $this->_query();
    }

    /**
     * @param $fields
     * @return bool|string
     */
    public function purchaseMasterpass($fields)
    {
        $this->_prepare(self::MODE_PURCHASE_MASTER_PASS, $fields);

        return $this->_query();
    }

    /**
     * @param array $fields
     * @return mixed
     */
    public function confirm3DS($fields)
    {
        $this->_prepare(self::MODE_CONFIRM_3DS, $fields);

        return $this->_query();
    }

    public function googlePay($fields)
    {
         $this->_prepare(self::MODE_GOOGLE_PAY, $fields);

        return $this->_query();
    }

    private function _query()
    {
        $postfields = http_build_query($this->_params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_apiUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $this->_server_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);


        return $server_output;
//      return json_decode($server_output);
    }


    /**
     * @param $action
     * @param array $params
     * @throws \InvalidArgumentException
     */
    private function _prepare($action, array $params)
    {
        $this->_action = $action;

        if (empty($params)) {
            throw new \InvalidArgumentException('Arguments must be not empty');
        }

        $this->_params = $params;

        $this->_params['signature'] = $this->_buildSignature();

        $this->_checkFields();
    }

    /**
     * Check required fields
     *
     * @param $fields
     * @return bool
     * @throws \InvalidArgumentException
     */
    private function _checkFields()
    {
        $required = $this->_getRequiredFields();
        $error = array();


        foreach ($required as $item) {
            if (array_key_exists($item, $this->_params)) {
                if (empty($this->_params[$item])) {
                    $error[] = $item;
                }
            } else {
                $error[] = $item;
            }
        }

        if (!empty($error)) {
            throw new \InvalidArgumentException('Missed required field(s): ' . implode(', ', $error) . '.');
        }

        return true;
    }

    /**
     * Required fields
     *
     * @return array
     */
    private function _getRequiredFields()
    {
        switch ($this->_action) {
            case self::MODE_PURCHASE:
                return array(
                    'operation',
                    'merchant_id',
                    'amount',
                    'signature',
                    'order_id',
                    'currency_iso',
                    'description',
                    'approve_url',
                    'decline_url',
                    'cancel_url',
                    'callback_url'
                );
            case self::MODE_REC_PAYMENT:
                return array(
                    'operation',
                    'merchant_id',
                    'amount',
                    'recurring_token',
                    'order_id',
                    'description',
                    'currency_iso',
                    'signature'
                );
            case self::MODE_VERIFY:
                return array(
                    'operation',
                    'merchant_id',
                    'order_id',
                    'approve_url',
                    'decline_url',
                    'cancel_url',
                    'signature'
                );
            case self::MODE_CHECK:
            case self::MODE_REVERSAL:
                return array(
                    'merchant_id',
                    'order_id',
                    'signature'
                );
            case self::MODE_COMPLETE:
                return array(
                    'merchant_id',
                    'operation',
                    'order_id',
                    'amount',
                    'signature'
                );
            case self::MODE_P2P_CREDIT:
                return array(
                    'operation',
                    'merchant_id',
                    'order_id',
                    'amount',
                    'card_number',
                    'currency_iso',
                    'signature'
                );
            case self::MODE_GET_BALANCE:
                return array(
                    'operation',
                    'merchant_id',
                    'date',
                    'signature'
                );
            case self::MODE_P2PDEBIT:
                return array(
                    'operation',
                    'merchant_id',
                    'order_id',
                    'amount',
                    'currency_iso',
                    'description',
                    'approve_url',
                    'decline_url',
                    'cancel_url',
                    'signature'
                );
            case self::MODE_PURCHASE_ON_MERCHANT:
                return array(
                    'operation',
                    'merchant_id',
                    'signature',
                    'order_id',
                    'amount',
                    'card_num',
                    'card_exp_month',
                    'card_exp_year',
                    'card_cvv',
                    'card_holder',
                    'payment_type',
                    'secure_type',
                    'currency_iso',
                    'description',
                    'callback_url',
                    'signature'
                );
            case self::MODE_GET_TOKEN_MASTER_PASS:
                return array(
                    'msisdn',
                    'client_id'
                );
            case self::MODE_PURCHASE_MASTER_PASS:
                return array(
                    'operation',
                    'merchant_id',
                    'amount',
                    'order_id',
                    'currency_iso',
                    'description',
                    'approve_url',
                    'decline_url',
                    'cancel_url',
                    'callback_url',
                    'add_params',
                    'signature'
                );

            case self::MODE_CONFIRM_3DS:
                return array(
                    'operation',
                    'transaction_key',
                    'merchant_id',
                    'd3ds_md',
                    'd3ds_pares',
                    'signature'
                );
            case self::MODE_GOOGLE_PAY:
                return array(
                    "operation",
                    "merchant_id",
                    "amount",
                    "order_id",
                    "currency_iso",
                    "recurring_token",
                    "description",
                    "callback_url",
                    "gpApiVersionMinor",
                    "gpApiVersion",
                    "gpPMDescription",
                    "gpPMType",
                    "gpPMICardNetwork",
                    "gpPMICardDetails",
                    "gpTokenType",
                    "gpToken"
                );
            default:
                throw new \InvalidArgumentException('Unknown transaction type');
        }
    }

    public function createSignature($fields)
    {

        return hash_hmac('md5', implode(self::FIELDS_DELIMITER, $fields), $this->_privateKey);


//        $this->_prepare($fields);
//
//        return $this->_buildSignature();
    }

    /**
     * Generate signature hash
     *
     * @param $fields
     * @return string
     * @throws \InvalidArgumentException
     */
    private function _buildSignature()
    {
        $signFields = $this->_getFieldsNameForSignature();
        $data = array();
        $error = array();

        foreach ($signFields as $item) {
            if (array_key_exists($item, $this->_params)) {
                $value = $this->_params[$item];
                if (is_array($value)) {
                    $data[] = implode(self::FIELDS_DELIMITER, $value);
                } else {
                    $data[] = (string)$value;
                }
            } else {
                $error[] = $item;
            }
        }

        if (!empty($error)) {
            throw new \InvalidArgumentException('Missed signature field(s): ' . implode(', ', $error) . '.');
        }
         return hash_hmac('md5', implode(self::FIELDS_DELIMITER, $data), $this->_privateKey);
    }


    /**
     * Signature fields
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    private function _getFieldsNameForSignature()
    {
        switch ($this->_action) {
            case self::MODE_PURCHASE:
                return array(
                    'merchant_id',
                    'order_id',
                    'amount',
                    'currency_iso',
                    'description'
                );
                break;
            case self::MODE_REC_PAYMENT:
                return array(
                    'merchant_id',
                    'order_id',
                    'amount',
                    'recurring_token',
                    'currency_iso',
                    "description"
                );
            case self::MODE_VERIFY:
                return array(
                    "merchant_id",
                    "order_id",
                    "amount",
                    "approve_url",
                    "decline_url",
                    "cancel_url"
                );
            case self::MODE_REVERSAL:
                return array(
                    "merchant_id",
                    "order_id"
                );
            case self::MODE_COMPLETE:
                return array(
                    "merchant_id",
                    "order_id",
                    "amount"
                );
            case self::MODE_CHECK:
                return array(
                    'merchant_id',
                    'order_id'
                );
            case self::MODE_P2P_CREDIT:
                return array(
                    'merchant_id',
                    'order_id',
                    'amount',
                    'card_number',
                    'currency_iso'
                );
            case self::MODE_GET_BALANCE:
                return array(
                    'merchant_id',
                    'date'
                );
            case self::MODE_P2PDEBIT:
                return array(
                    'merchant_id',
                    'order_id',
                    'amount',
                    'currency_iso',
                    'description',
                    'approve_url',
                    'decline_url',
                    'cancel_url'
                );
            case self::MODE_PURCHASE_MASTER_PASS:
            case self::MODE_GOOGLE_PAY:
            case self::MODE_PURCHASE_ON_MERCHANT:
                return array(
                    "merchant_id",
                    "order_id",
                    "amount",
                    "currency_iso",
                    "description"
                );
            case self::MODE_CONFIRM_3DS:
                return array(
                    "merchant_id",
                    "transaction_key",
                    "d3ds_md",
                    "d3ds_pares"
                );
            case self::MODE_GET_TOKEN_MASTER_PASS :
                return array();
            default:
                throw new \InvalidArgumentException('Unknown transaction type: ' . $this->_action);
        }
    }

    public function redirect_post($url, array $data, array $headers = null)
    {
        $data['signature'] = $this->signature($data);

        $params = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        if (!is_null($headers)) {
            $params['http']['header'] = '';
            foreach ($headers as $k => $v) {
                $params['http']['header'] .= "$k: $v\n";
            }
        }
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if ($fp) {
            echo @stream_get_contents($fp);
            die();
        } else {
            // Error
            throw new Exception("Error loading '$url', $php_errormsg");
        }
    }
}